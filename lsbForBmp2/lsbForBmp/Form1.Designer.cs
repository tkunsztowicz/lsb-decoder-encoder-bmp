﻿namespace lsbForBmp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbInputPicture = new System.Windows.Forms.PictureBox();
            this.pbOutputPicture = new System.Windows.Forms.PictureBox();
            this.bReadFile = new System.Windows.Forms.Button();
            this.bInsert = new System.Windows.Forms.Button();
            this.bReadInfo = new System.Windows.Forms.Button();
            this.bSaveModifiedFile = new System.Windows.Forms.Button();
            this.tbEncryptKey = new System.Windows.Forms.TextBox();
            this.tbStegKey = new System.Windows.Forms.TextBox();
            this.labelEncode = new System.Windows.Forms.Label();
            this.labelKey = new System.Windows.Forms.Label();
            this.tbToEncrypt = new System.Windows.Forms.TextBox();
            this.tbEncrypted = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbInputPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbOutputPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // pbInputPicture
            // 
            this.pbInputPicture.Location = new System.Drawing.Point(12, 12);
            this.pbInputPicture.Name = "pbInputPicture";
            this.pbInputPicture.Size = new System.Drawing.Size(640, 480);
            this.pbInputPicture.TabIndex = 0;
            this.pbInputPicture.TabStop = false;
            // 
            // pbOutputPicture
            // 
            this.pbOutputPicture.Location = new System.Drawing.Point(658, 12);
            this.pbOutputPicture.Name = "pbOutputPicture";
            this.pbOutputPicture.Size = new System.Drawing.Size(640, 480);
            this.pbOutputPicture.TabIndex = 1;
            this.pbOutputPicture.TabStop = false;
            // 
            // bReadFile
            // 
            this.bReadFile.Location = new System.Drawing.Point(12, 516);
            this.bReadFile.Name = "bReadFile";
            this.bReadFile.Size = new System.Drawing.Size(125, 23);
            this.bReadFile.TabIndex = 2;
            this.bReadFile.Text = "Wczytaj plik";
            this.bReadFile.UseVisualStyleBackColor = true;
            this.bReadFile.Click += new System.EventHandler(this.bReadFile_Click);
            // 
            // bInsert
            // 
            this.bInsert.Location = new System.Drawing.Point(12, 545);
            this.bInsert.Name = "bInsert";
            this.bInsert.Size = new System.Drawing.Size(125, 23);
            this.bInsert.TabIndex = 3;
            this.bInsert.Text = "Wstaw";
            this.bInsert.UseVisualStyleBackColor = true;
            this.bInsert.Click += new System.EventHandler(this.bInsert_Click);
            // 
            // bReadInfo
            // 
            this.bReadInfo.Location = new System.Drawing.Point(12, 574);
            this.bReadInfo.Name = "bReadInfo";
            this.bReadInfo.Size = new System.Drawing.Size(125, 23);
            this.bReadInfo.TabIndex = 4;
            this.bReadInfo.Text = "Odczytaj info";
            this.bReadInfo.UseVisualStyleBackColor = true;
            this.bReadInfo.Click += new System.EventHandler(this.bReadInfo_Click);
            // 
            // bSaveModifiedFile
            // 
            this.bSaveModifiedFile.Location = new System.Drawing.Point(12, 603);
            this.bSaveModifiedFile.Name = "bSaveModifiedFile";
            this.bSaveModifiedFile.Size = new System.Drawing.Size(125, 23);
            this.bSaveModifiedFile.TabIndex = 5;
            this.bSaveModifiedFile.Text = "Zapisz plik";
            this.bSaveModifiedFile.UseVisualStyleBackColor = true;
            this.bSaveModifiedFile.Click += new System.EventHandler(this.bSaveModifiedFile_Click);
            // 
            // tbEncryptKey
            // 
            this.tbEncryptKey.Location = new System.Drawing.Point(276, 518);
            this.tbEncryptKey.Name = "tbEncryptKey";
            this.tbEncryptKey.Size = new System.Drawing.Size(317, 20);
            this.tbEncryptKey.TabIndex = 6;
            // 
            // tbStegKey
            // 
            this.tbStegKey.Location = new System.Drawing.Point(276, 547);
            this.tbStegKey.Name = "tbStegKey";
            this.tbStegKey.Size = new System.Drawing.Size(317, 20);
            this.tbStegKey.TabIndex = 7;
            // 
            // labelEncode
            // 
            this.labelEncode.AutoSize = true;
            this.labelEncode.Location = new System.Drawing.Point(184, 521);
            this.labelEncode.Name = "labelEncode";
            this.labelEncode.Size = new System.Drawing.Size(86, 13);
            this.labelEncode.TabIndex = 8;
            this.labelEncode.Text = "Hasło szyfrujące";
            // 
            // labelKey
            // 
            this.labelKey.AutoSize = true;
            this.labelKey.Location = new System.Drawing.Point(237, 550);
            this.labelKey.Name = "labelKey";
            this.labelKey.Size = new System.Drawing.Size(33, 13);
            this.labelKey.TabIndex = 9;
            this.labelKey.Text = "Klucz";
            // 
            // tbToEncrypt
            // 
            this.tbToEncrypt.Location = new System.Drawing.Point(683, 519);
            this.tbToEncrypt.Name = "tbToEncrypt";
            this.tbToEncrypt.Size = new System.Drawing.Size(608, 20);
            this.tbToEncrypt.TabIndex = 10;
            // 
            // tbEncrypted
            // 
            this.tbEncrypted.Location = new System.Drawing.Point(683, 548);
            this.tbEncrypted.Name = "tbEncrypted";
            this.tbEncrypted.Size = new System.Drawing.Size(608, 20);
            this.tbEncrypted.TabIndex = 11;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1303, 700);
            this.Controls.Add(this.tbEncrypted);
            this.Controls.Add(this.tbToEncrypt);
            this.Controls.Add(this.labelKey);
            this.Controls.Add(this.labelEncode);
            this.Controls.Add(this.tbStegKey);
            this.Controls.Add(this.tbEncryptKey);
            this.Controls.Add(this.bSaveModifiedFile);
            this.Controls.Add(this.bReadInfo);
            this.Controls.Add(this.bInsert);
            this.Controls.Add(this.bReadFile);
            this.Controls.Add(this.pbOutputPicture);
            this.Controls.Add(this.pbInputPicture);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pbInputPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbOutputPicture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbInputPicture;
        private System.Windows.Forms.PictureBox pbOutputPicture;
        private System.Windows.Forms.Button bReadFile;
        private System.Windows.Forms.Button bInsert;
        private System.Windows.Forms.Button bReadInfo;
        private System.Windows.Forms.Button bSaveModifiedFile;
        private System.Windows.Forms.TextBox tbEncryptKey;
        private System.Windows.Forms.TextBox tbStegKey;
        private System.Windows.Forms.Label labelEncode;
        private System.Windows.Forms.Label labelKey;
        private System.Windows.Forms.TextBox tbToEncrypt;
        private System.Windows.Forms.TextBox tbEncrypted;
    }
}

