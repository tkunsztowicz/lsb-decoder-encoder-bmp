﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace lsbForBmp
{
    class RandomList
    {
        private List<Int32> list = new List<Int32>();

        public RandomList(Int32 seed, Int32 length, Int32 data_len)
        {
            Random rand = new Random(seed);
            List<Int32> tmp_list = new List<Int32>();
            Int32 tmp_list_length = 0;
            Int32 list_length = 0;

            for (int i = 0; i < length; i++)
            {
                // fill the tmp list
                tmp_list.Add(i);
                tmp_list_length++;
            }

            while (list_length < data_len)
            {
                Int32 idx = rand.Next(0, tmp_list_length);
                Int32 val = tmp_list.ElementAt(idx);
                if (!list.Contains(val))
                {
                    list.Add(val);
                    tmp_list.RemoveAt(idx);
                    list_length++;
                    tmp_list_length--;
                }
            }
        }

        public List<Int32> getRandomList()
        {
            return list;
        }
    }
}
