﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Security.Cryptography;

namespace lsbForBmp
{
    public partial class Form1 : Form
    {
        String key = "";
        List<Int32> rand_idx_list;

        public Form1()
        {
            InitializeComponent();
        }

        private void bReadFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();

            dlg.Title = "Open Image";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                pbInputPicture.Image = Image.FromFile(dlg.FileName);
            }
        }

        private void bInsert_Click(object sender, EventArgs e)
        {
            key = tbEncryptKey.Text;
            String data = tbToEncrypt.Text;

            byte[] enc_data = encryptData(data, key);
            byte[] fixed_enc_data = getFixedData(ref enc_data);

            Bitmap bmp = (Bitmap)pbInputPicture.Image;

            // generate random list with image pixel indexes
            RandomList rand_list = new RandomList(Convert.ToInt32(tbStegKey.Text), bmp.Height * bmp.Width, calculateCountOfPixelsToHideData(fixed_enc_data));
            // get generated list of indexes
            rand_idx_list = rand_list.getRandomList();

            // image - encrypted info hiding
            Image img = MyImageExtension.hideInfoInImage((Image)bmp, fixed_enc_data, ref rand_idx_list);
            pbOutputPicture.Image = img;
            
        }

        private byte[] getFixedData(ref byte[] data)
        {
            // redundant (*5) resizing of data
            byte[] out_data = new byte[data.Length * 5];
            Int32 out_data_len = out_data.Length;
            Int32 data_len = data.Length;
            Int32 byte_idx = 0;
            Int32 bit_idx = 0;

            
            for (int i = 0; i < data_len; i++)
            {
                // set 5 output bytes for 1 byte of input
                MyImageExtension.setFiveBytes(ref out_data, data[i], ref byte_idx, ref bit_idx);
            }
            
            return out_data;
        }


        private int calculateCountOfPixelsToHideData(byte[] data)
        {
            int pixelsCount = 0;

            pixelsCount = (int)Math.Ceiling((double)data.Length * 8 / 3);
            
            return pixelsCount;
        }
        

        private void copyArray(byte[] src, Int32 src_idx, byte[] dest, Int32 dest_idx, Int32 length)
        {
            Int32 j = 0;
            for (int i = dest_idx; i < dest_idx + length; i++)
            {
                dest[i] = src[src_idx + j];
                ++j;
            }
        }

        private byte[] formData(byte[] data)
        {
            Int32 size = data.Length;
            String sSize = size.ToString();
            byte[] byte_size = Encoding.ASCII.GetBytes(sSize);
            byte[] output = { };

            Array.Copy(byte_size, 0, output, 0, size);
            Array.Copy(data, size, output, 0, data.Length);

            return output;
        }

        private byte[] encryptData(String data, String key)
        {
            Int32 data_len = data.Length;
            String tmp_data = data_len.ToString() + "|" + data;
            byte[] byte_data = Encoding.ASCII.GetBytes(tmp_data);
            byte[] byte_key = Encoding.ASCII.GetBytes(key);
            byte[] hash_key;
            byte[] out_data = { };

            SHA512 shaM = new SHA512Managed();
            hash_key = shaM.ComputeHash(byte_key);

            byte[] Key = new byte[32];
            byte[] IV = new byte[16];

            Array.Copy(hash_key, 0, Key, 0, 32);
            Array.Copy(hash_key, 32, IV, 0, 16);


            out_data = encryptStream(byte_data, Key, IV);

            return out_data;
        }

        private static byte[] encryptStream(byte[] plain, byte[] Key, byte[] IV)
        {
            byte[] encrypted; ;
            using (MemoryStream mstream = new MemoryStream())
            {
                using (AesCryptoServiceProvider aesProvider = new AesCryptoServiceProvider())
                {
                    aesProvider.Mode = CipherMode.CBC;
                    aesProvider.Padding = PaddingMode.Zeros;
                    using (CryptoStream cryptoStream = new CryptoStream(mstream,
                        aesProvider.CreateEncryptor(Key, IV), CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(plain, 0, plain.Length);
                    }
                    encrypted = mstream.ToArray();
                }
            }
            return encrypted;
        }

        private byte[] getDecryptedData(byte[] data, ref Int32 msg_start_idx)
        {
            Int32 data_len = 0;

            // check data length
            Int32 i = 0;
            while (i < data.Length)
            {
                if (data[i] == '|')
                {
                    msg_start_idx = i + 1;
                    break;
                }
                ++i;
            }
            byte[] tmp_data_len = new byte[msg_start_idx - 1];

            Array.Copy(data, 0, tmp_data_len, 0, (msg_start_idx - 1));
            data_len = Convert.ToInt32(Encoding.ASCII.GetString(tmp_data_len));

            byte[] out_data = new byte[data_len];
            Array.Copy(data, msg_start_idx, out_data, 0, data_len);

            return out_data;
        }
   

        private byte[] decryptData(byte[] byte_data, String key)
        {
            byte[] byte_key = Encoding.ASCII.GetBytes(key);
            byte[] hash_key;
            byte[] out_data = { };

            SHA512 shaM = new SHA512Managed();
            hash_key = shaM.ComputeHash(byte_key);

            byte[] Key = new byte[32];
            byte[] IV = new byte[16];

            Array.Copy(hash_key, 0, Key, 0, 32);
            Array.Copy(hash_key, 32, IV, 0, 16);

            out_data = mydec(byte_data, Key, IV);
            String test = Encoding.ASCII.GetString(out_data);

            // check the length of the string
            Int32 msg_start_idx = 0;

            return getDecryptedData(out_data, ref msg_start_idx); 
        }

        public static byte[] mydec(byte[] encrypted, byte[] Key, byte[] IV)
        {
            byte[] plain;
            using (MemoryStream mStream = new MemoryStream(encrypted)) //add encrypted
            {
                using (AesCryptoServiceProvider aesProvider = new AesCryptoServiceProvider())
                {
                    aesProvider.Mode = CipherMode.CBC;
                    aesProvider.Padding = PaddingMode.Zeros;
                    using (CryptoStream cryptoStream = new CryptoStream(mStream,
                        aesProvider.CreateDecryptor(Key, IV), CryptoStreamMode.Read))
                    {
                        //cryptoStream.Read(encrypted, 0, encrypted.Length);
                        using (StreamReader stream = new StreamReader(cryptoStream))
                        {
                            string sf = stream.ReadToEnd();
                            plain = System.Text.Encoding.Default.GetBytes(sf);
                        }
                    }
                }
            }
            return plain;
        }

        private void bSaveModifiedFile_Click(object sender, EventArgs e)
        {
            pbOutputPicture.Image.Save("C:/Users/Konrad/Desktop/copy.png");
        }

        private void bReadInfo_Click(object sender, EventArgs e)
        {
            Image img = pbOutputPicture.Image;

            // get hidden info from image
            byte[] hiddenInfo = MyImageExtension.GetHiddenInfoFromImage(img, ref rand_idx_list);
            byte[] fixed_hidden_info = MyImageExtension.decodeDataByFive(ref hiddenInfo);

            byte[] dec_data = decryptData(fixed_hidden_info, key);
            tbEncrypted.Text = System.Text.Encoding.UTF8.GetString(dec_data);
        }
    }

    static class MyImageExtension
    {
        // values understood as 1 and 0
        // 11111
        private const byte one = 31;
        // 10101
        private const byte zero = 21;

        private static void SetBitOfPixel(ref int X, ref Int32 byte_idx, ref int bit_idx, ref byte[] data)
        {
            // if data_len was reached do nothing
            if (byte_idx >= data.Length)
            {
                return;
            }

            // reset least significant bit
            X = X - X % 2;

            if (bit_idx >= 8)
            {
                bit_idx = 0; // first bit
                byte_idx++; // of next byte
            }

            // dont set bit if last byte was written
            if (!(byte_idx >= data.Length))
            {
                X = X + GetBit(data[byte_idx], bit_idx);
                bit_idx++; // set next bit to get
            }
        }

        public static Image hideInfoInImage(this Image img, byte[] data, ref List<Int32> rand_idx_list)
        {
            Bitmap bmp = (Bitmap)img;
            Bitmap bmp_out = (Bitmap)bmp.Clone();
            int Height = bmp.Height;
            int Width = bmp.Width;
            Int32 data_len = data.Length;
            int bit_idx = 0;
            Int32 byte_idx = 0;

            int requiredPixelCount = rand_idx_list.Count();

            // hold pixel elements
            int R = 0, G = 0, B = 0;

            if ((Width * Height) < requiredPixelCount)
            {
                return (Image)img.Clone(); // return clone of the original if too less place
            }

            // pass through all elements of pixel_idx list
            foreach (int pixel_num in rand_idx_list)
            {
                // get row and column from list
                int row = pixel_num / Width; ;
                int col = pixel_num % Width; ;

                if (byte_idx >= data_len)
                {
                    break;
                }

                // get pixel
                Color pixel = bmp.GetPixel(col, row); // x - width, y - height
                R = pixel.R;
                G = pixel.G;
                B = pixel.B;

                SetBitOfPixel(ref R, ref byte_idx, ref bit_idx, ref data);
                SetBitOfPixel(ref G, ref byte_idx, ref bit_idx, ref data);
                SetBitOfPixel(ref B, ref byte_idx, ref bit_idx, ref data);

                // set pixel of output bitmap
                bmp_out.SetPixel(col, row, Color.FromArgb(R, G, B));
            }

            // set output
            return (Image)bmp_out;

        }


        public static byte[] GetHiddenInfoFromImage(this Image img, ref List<Int32> rand_idx_list)
        {
            Bitmap bmp = (Bitmap)img;
            int Height = bmp.Height;
            int Width = bmp.Width;
            int data_len = (int)Math.Floor((double)rand_idx_list.Count() * 3 / 8);
            byte[] out_data = new byte[data_len];
            int bit_idx = 0;
            Int32 byte_idx = 0;

            // hold pixel elements
            int R = 0, G = 0, B = 0;

            foreach (int pixel_num in rand_idx_list)
            {
                // get row and column from list
                int row = pixel_num / Width; ;
                int col = pixel_num % Width; ;

                // get pixel
                Color pixel = bmp.GetPixel(col, row); // x - width, y - height
                R = pixel.R;
                G = pixel.G;
                B = pixel.B;

                SetBit(ref out_data, R, ref bit_idx, ref byte_idx);
                SetBit(ref out_data, G, ref bit_idx, ref byte_idx);
                SetBit(ref out_data, B, ref bit_idx, ref byte_idx);
            }

            // set output
            return out_data;

        }

        private static void SetBit(ref byte[] data, int X, ref int bit_idx, ref Int32 byte_idx)
        {
            if (bit_idx >= 8)
            {
                bit_idx = 0; // first bit
                byte_idx++; // of next byte
            }

            if (byte_idx >= data.Length)
            {
                return;
            }

            data[byte_idx] |= Convert.ToByte((GetBit((byte)X, 0) << bit_idx));
            bit_idx++;
            
        }

        private static void SetBit2(ref byte[] data, int X, ref int bit_idx, ref Int32 byte_idx, int bound_idx)
        {
            // input val into out_data
            for (int i = 0; i <= bound_idx; i++)
            {
                if (bit_idx >= 8)
                {
                    bit_idx = 0; // first bit
                    byte_idx++; // of next byte
                }

                data[byte_idx] |= Convert.ToByte((GetBit((byte)X, i) << bit_idx));
                bit_idx++;
            }
        }

        // get specific bit from byte
        private static int GetBit(this byte b, int bitNumber)
        {
            bool value = (b & (1 << bitNumber)) != 0;
            return Convert.ToInt32(value);
        }

        private static void SetBit3(ref byte[] data, byte val, ref int bit_idx, ref Int32 byte_idx)
        {
            byte set_val = 0;

            // check if one or zero
            if (val == one)
            {
                set_val = 1;
            }
            else if (val == zero)
            {
                set_val = 0;
            }

            if (bit_idx >= 8)
            {
                bit_idx = 0; // first bit
                byte_idx++; // of next byte
            }

            if (!(byte_idx >= data.Length))
            {
                data[byte_idx] |= Convert.ToByte(set_val << bit_idx);
            }
            
            bit_idx++;
        }

        public static void setFiveBytes(ref byte[] out_data, byte data_i, ref Int32 byte_idx, ref Int32 bit_idx)
        {
            const int bit_count = 8;
            for (int i = 0; i < bit_count; i++)
            {
                int cur_bit = GetBit(data_i, i);
                int val = 0;

                if (cur_bit == 1)
                {
                    val = one;
                }
                else // = 0
                {
                    val = zero;
                }

                SetBit2(ref out_data, val, ref bit_idx, ref byte_idx, 4);
            }
        }

        public static byte[] decodeDataByFive(ref byte[] data)
        {
            int data_len = data.Length;
            byte[] out_data = new byte[data_len / 5];
            int out_data_len = out_data.Length;
            int byte_idx = 0;
            int bit_idx = 0;
            int bit_num = 5;
            int out_byte_idx = 0;
            int out_bit_idx = 0;

            // pass through all bytes starting from last and read message
            
            while (out_byte_idx < out_data_len)
            {
                int val = checkValueByBitsNum(ref data, ref byte_idx, ref bit_idx, bit_num);
                SetBit3(ref out_data, (byte)val, ref out_bit_idx, ref out_byte_idx);
            }

            return out_data;
        }

        private static byte checkValueByBitsNum(ref byte[] data, ref int byte_idx, ref int bit_idx, int bit_num)
        {
            int val = 0;

            for (int i = 0; i < bit_num; i++)
            {
                if (bit_idx >= 8)
                {
                    bit_idx = 0; // first bit
                    byte_idx++; // of previous byte
                }

                if (!(byte_idx >= data.Length))
                {
                    // calculate val
                    val |= (GetBit(data[byte_idx], bit_idx) << i);
                }
                // increment bit number
                bit_idx++;
            }

            return (byte)val;
        }
    }

}




